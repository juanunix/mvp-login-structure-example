package com.example.pgirardi.loginmvp.features.login.interfaces;

/**
 * Created by p.girardi on 6/27/2016.
 */

public interface ILoginModelUser {
    boolean checkUserValidity(String name, String passwd);
}
